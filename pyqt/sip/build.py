# Usage: python build.py [qt_dir] [python_dir]

import os
import sys
import sipconfig
from PyQt5 import QtCore

qt_inc_dir = sys.argv[1] + r"\include"
qt_lib_dir = sys.argv[1] + r"\lib"
sip_inc_dir = sys.argv[2] + r"\sip\PyQt5"
sip_bin = sys.argv[2] + r"\sip.exe"
python_inc_dir = sys.argv[2] + r"\include"

build_file = "qtermwidget.sbf"

config = sipconfig.Configuration()

os.system(" ".join([sip_bin, "-c", ".", "-b", build_file,
                    "-I" + sip_inc_dir, QtCore.PYQT_CONFIGURATION["sip_flags"],
                    "qtermwidget.sip"]))

makefile = sipconfig.SIPModuleMakefile(config, build_file)

extraFlags = r"-I..\..\lib -I..\..\build\lib -I%s -I%s/QtCore -I%s/QtGui -I%s/QtWidgets -I%s" % (qt_inc_dir, qt_inc_dir, qt_inc_dir, qt_inc_dir, python_inc_dir)
makefile.extra_cflags = [extraFlags]
makefile.extra_cxxflags = [extraFlags]
makefile.extra_libs = [r"..\..\build\qtermwidget5", qt_lib_dir + r"\Qt5Widgets", qt_lib_dir + r"\Qt5Core", qt_lib_dir + r"\Qt5Gui"]

makefile.generate()