# Set Visual Studio Variables
pushd 'C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build'
cmd /c "vcvarsall.bat x86 & set" |
foreach {
  if ($_ -match "=") {
    $v = $_.split("="); set-item -force -path "ENV:\$($v[0])"  -value "$($v[1])"
  }
}
popd

$toppath = Get-Location

mkdir qt
cd qt
$qtpath = Get-Location
curl.exe -L -o qt.zip https://gitlab.com/anaxium1/qt5/-/jobs/artifacts/master/raw/qt.zip?job=job
7z x qt.zip
rm qt.zip

cd $toppath
mkdir python
cd python
$pythonpath = Get-Location
Copy-Item -Path "C:\Python36-32\*" -Destination $pythonpath -Recurse
curl.exe -L -o sip.zip https://gitlab.com/anaxium1/sip/-/jobs/artifacts/master/raw/sip.zip?job=job
curl.exe -L -o pyqt5.zip https://gitlab.com/anaxium1/pyqt5/-/jobs/artifacts/master/raw/pyqt5.zip?job=job
curl.exe -L -o pyqt5_sip.zip https://gitlab.com/anaxium1/pyqt5_sip/-/jobs/artifacts/master/raw/pyqt5_sip.zip?job=job
7z x sip.zip
7z x pyqt5.zip
7z x pyqt5_sip.zip
$env:Path = "$pythonpath;$env:Path"

cd $toppath
mkdir lxqt-build-tools
cd lxqt-build-tools
curl.exe -L -o lxqt-build-tools.zip https://gitlab.com/anaxium1/lxqt-build-tools/-/jobs/artifacts/master/raw/lxqt-build-tools.zip?job=job
7z x -y lxqt-build-tools.zip

cd $toppath
mkdir utf8proc
cd utf8proc
curl.exe -L -o utf8proc.zip https://gitlab.com/anaxium1/utf8proc/-/jobs/artifacts/master/raw/utf8proc.zip?job=job
7z x -y utf8proc.zip

cd $toppath
mkdir build
cd build
cmake "-Dlxqt-build-tools_DIR=$toppath\lxqt-build-tools\share\cmake\lxqt-build-tools" "-DQt5Widgets_DIR=$qtpath\lib\cmake\Qt5Widgets" "-DQt5LinguistTools_DIR=$qtpath\lib\cmake\Qt5LinguistTools" "-DQt5Network_DIR=$qtpath\lib\cmake\Qt5Network" -DUSE_UTF8PROC=ON "-DUtf8Proc_DIR=$toppath\utf8proc" -DCMAKE_BUILD_TYPE=Release -G "NMake Makefiles" ..
nmake

$env:Path = "$qtpath\bin;$env:Path"
cd $toppath
cd pyqt\sip
python build.py $qtpath $pythonpath
nmake

cd $toppath
mkdir out
cd out
$outpath = Get-Location
Copy-Item -Path "$toppath\build\qtermwidget5.dll" -Destination "$outpath" -Recurse
Copy-Item -Path "$toppath\lib\color-schemes" -Destination "$outpath" -Recurse
Copy-Item -Path "$toppath\pyqt\sip\QTermWidget.pyd" -Destination "$outpath" -Recurse
7z a -r ..\qtermwidget.zip *
